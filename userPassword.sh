#!/bin/bash
# Name: UserPassword.sh - Join 2 Textfiles
#
# Benutzung:
# joinPassword.sh file1 file2
#
# Autor: Michelangelo Megna
# Datum: 24.10.2021
join -1 3 -2 1 $1 $2 > ./testfiles/UserUndPw.txt
