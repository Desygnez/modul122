#!/bin/bash
#Erraten von Zahlen
#Autor: Michelangelo Megna
#Version: 1.0, Datum: 16.10.2021

echo "Erraten sie eine Nummer zwischen 1 und 20"
finished=false

random=$[$RANDOM%20]

until [ $finished = true ]; do
  read eingabe
  if [[ $eingabe -gt $random ]]
  then
    echo "Die Zahl ist kleiner als $eingabe"
  elif [ $random -gt $eingabe ]; then
    echo "Die Zahl ist grösser als $eingabe"
  else
    echo "Das war die richtige Zahl!"
    finished=true
  fi
done