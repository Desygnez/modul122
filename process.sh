#!/bin/bash
# Name: process - Checks if process is used
#
# Autor: Michelangelo Megna
# Datum: 28.10.2021
if ps | grep-q $1
then
  echo "The process $1 is used."
else
  echo "The process $1 is free."
fi
