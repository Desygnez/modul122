# vi /m122/scripts/prozessalerting.sh

#!/bin/bash

#######################################################################################
#Scriptname    :prozessalerting.sh
#Description   :send alert mail when server memory is running low      
#Author        :Michelangelo Megna
#Email         :michelangelo.megna@lernende.bbw.ch
#######################################################################################
##email variables
##email subject 
subject="Prozess Memory Status Alert"
##sending mail as
from="process.monitor@example.com"
## sending mail to
to="michelangelo.megna@lernende.bbw.ch"

## get total memory
free=$(free -mt | grep Total | awk '{print $4}')

## check free memory
if [[ "$free" -le 100  ]]; then
        ## get processes using system memory and save file 
        ps -eo pid,ppid,cmd,%mem,%cpu --sort=-%mem | head >/tmp/process_memory.txt
        file=C:\Users\mmegna\OneDrive\BBW IT\3. Lehrjahr\Modul122\process_memory.txt

         ## send email when memory low
        echo -e "Danger, memory is low!\n\nFree memory: $free MB" | mailx -a "$file" -s "$subject" -r "$from" -c "$to" "$also_to"
fi

exit 0
