#!/bin/bash
#Programm: Umgestalten von daten
#Autor: Michelangelo Megna
#Version: 1.0, Datum: 16.10.2021

if test $# -ne 2; then

    echo "Sie müssen einen Namen als Argument eingeben!"

    echo "Usage: ./umgestalten.sh inputFile outputFile"

    exit 1

fi

if [ ! -f "$1" ]; then

    echo "$1 does not exist."

    exit 1

fi

if [ ! -f "$2" ]; then

    echo "$2 does not exist."

    exit 1

fi

grep -w "2  [0-9][  ,0-9]" $1 | cut -f 3,4 | tr -d "'" | sort -k 1 -V >$2